const filmsNd = document.querySelector('.films');

const getFilms = async () => {
    const filmsApi = await fetch('http://localhost:4000/films');
    const filmsRes = await filmsApi.json();
    mostrarFilms(filmsRes)//console.log(filmsRes);//Aquí podemos reemplazarlo por un método mostrarImáges(Imprimirlas como la pokeApi)
}

const mostrarFilms = (films) => {//Los cambios se hacen en el server, recuerda que debes estar en la carpeta del server para hacer el comando 'npm run dev' para activar el servidor
    for (const film of films) {
        const titleNd= document.createElement("h1");
        const yearNd = document.createElement("h2");
        const imgNd = document.createElement("img");

        titleNd.textContent = `${film.name}`;
        yearNd.textContent =`${film.year}`;
        imgNd.src = `${film.poster}`;

        filmsNd.appendChild(titleNd);
        filmsNd.appendChild(yearNd);
        filmsNd.appendChild(imgNd);
    }
}

getFilms();