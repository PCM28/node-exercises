const express = require('express');
const cors = require('cors');//--------------Es nuevo
const PORT = 4000;

const server = express();
const router = express.Router();

router.get('/films',(req,res)=>{
    const films = [
        {
            name:"Jobs",
            year:"2013",
            poster:"https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.vhp_QBGFQcyXB6oEtx677gHaE8%26pid%3DApi&f=1"
        },
        {
            name:"The Wolf of Wall Street",
            year:"2013",
            poster:"https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.bLVd9bJzxsJO26l0CX5yGAHaE8%26pid%3DApi&f=1"
        }
    ]
    res.send(films);
});

server.use(
    cors({
        origin: '*',
        credentials: true
    })
)

server.use("/",router);

server.listen(PORT,()=>{
    console.log(`Server running in http://localhost:${PORT}`);
});